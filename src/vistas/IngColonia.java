package vistas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class IngColonia extends JDialog {
    private JPanel contentPane;
    private JTextField txtNombreC;
    private JTextField txtUbicacionC;
    private JTextField txtExTerriC;
    private JTextField txtNoFamC;
    private JTextField textField5;
    private JButton guardarButton;
    private JButton guardarButton1;
    private JButton buttonOK;
    private JButton buttonCancel;

    public IngColonia() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocation(400,100);
        this.setMinimumSize(new Dimension(600,350));



        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        guardarButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarColonia();
                limpiarCajas();
            }
        });
    }
    public void IngresarColonia(){
       Colonia NeCol = new Colonia();
        CtrlFunciones cf = new CtrlFunciones();
        NeCol.setNombre(txtNombreC.getText());
        NeCol.setUbicacion(txtUbicacionC.getText());
        NeCol.setExtencionTerritorial(txtExTerriC.getText());
        NeCol.setNoFamilias(Integer.parseInt(txtNoFamC.getText()));

        cf.AgregarColonia(NeCol);
        cf.mostrarColonia();
        JOptionPane.showMessageDialog(this, "Colonia ingresada");
    }
    public void limpiarCajas(){
        txtNombreC.setText("");
        txtUbicacionC.setText("");
        txtExTerriC.setText("");
        txtNoFamC.setText("");
    }
    private void onOK() {
        // add your code here
        //dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        IngColonia dialog = new IngColonia();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
