package vistas;

/**
 * Created by emr on 11/07/2017.
 */
public class Filtro {
    private int codigo;
    private String estado;
    private Double presionAgua;
    private String Clasificacion;

    public Filtro() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getPresionAgua() {
        return presionAgua;
    }

    public void setPresionAgua(Double presionAgua) {
        this.presionAgua = presionAgua;
    }

    public String getClasificacion() {
        return Clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        Clasificacion = clasificacion;
    }
}
