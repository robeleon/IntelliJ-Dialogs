package vistas;

/**
 * Created by emr on 11/07/2017.
 */
public class Colonia {
    private String Nombre;
    private String Ubicacion;
    private String ExtencionTerritorial;
    private int NoFamilias;

    public Colonia() {
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public String getExtencionTerritorial() {
        return ExtencionTerritorial;
    }

    public void setExtencionTerritorial(String extencionTerritorial) {
        ExtencionTerritorial = extencionTerritorial;
    }

    public int getNoFamilias() {
        return NoFamilias;
    }

    public void setNoFamilias(int noFamilias) {
        NoFamilias = noFamilias;
    }
}
