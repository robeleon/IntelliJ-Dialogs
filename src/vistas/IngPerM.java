package vistas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;

public class IngPerM extends JDialog {
    private JPanel contentPane;
    private JTextField txtNombre;
    private JTextField txtApellido;
    private JTextField txtEdad;
    private JTextField txtDireccion;
    private JButton btnAgregar;
    private JButton Mostrar;
    private JTable tbFiltros;
    private JButton buttonOK;
    private JButton buttonCancel;
private DefaultTableModel tbFtr;
    public IngPerM() {
        setLocation(400,50);
        this.setMinimumSize(new Dimension(600,400));
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        this.setLocationRelativeTo(null);
        tbFtr = new DefaultTableModel();
        tbFtr.addColumn("Nombre");
        tbFtr.addColumn("Apellido");
        tbFtr.addColumn("Edad");
        tbFtr.addColumn("Direccion");
        tbFiltros.setModel(tbFtr);

        getRootPane().setDefaultButton(buttonOK);
       /* buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });*/

/*        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });*/

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        btnAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               /* PerMonitora perM = new PerMonitora();
                CtrlFunciones contrFunc = new CtrlFunciones();
                perM.setNombre(txtNombre.getText());
                perM.setApellido(txtApellido.getText());
                perM.setEdad(txtEdad.getText());
                perM.setDireccion(txtEdad.getText());
                contrFunc.AgregarPerM(perM);
                contrFunc.mostrarPerM();*/
                IngresarPer();

                limpiarCajas();
            }
        });
        Mostrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CtrlFunciones cF = new CtrlFunciones();
                cF.pruebaDt();
                cF.mostrarPerM();
            }
        });
        Mostrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tbFtr.removeRow(tbFiltros.getSelectedRow());
            }
        });
    }

public void obtenerdeLista(){
       PerMonitora pe;
    pe = new PerMonitora();
    tbFtr.addRow(new Object[]{pe.getNombre(),pe.getApellido(),pe.getEdad(),pe.getDireccion()});
}
    public void IngresarPer(){
 PerMonitora perM = new PerMonitora();
    CtrlFunciones contrFunc = new CtrlFunciones();
 perM.setNombre(txtNombre.getText());
 perM.setApellido(txtApellido.getText());
 perM.setEdad(txtEdad.getText());
 perM.setDireccion(txtEdad.getText());
 tbFtr.addRow(new Object[]{perM.getNombre(),perM.getApellido(),perM.getEdad(),perM.getDireccion()});
     contrFunc.AgregarPerM(perM);
     contrFunc.mostrarPerM();
    JOptionPane.showMessageDialog(this, "Persona ingresada");
}
public void limpiarCajas(){
        txtNombre.setText("");
        txtApellido.setText("");
        txtEdad.setText("");
        txtDireccion.setText("");
}
    private void onOK() {
        // add your code here
        //dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        IngPerM dialog = new IngPerM();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);

    }
}
