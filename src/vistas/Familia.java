package vistas;

/**
 * Created by emr on 11/07/2017.
 */
public class Familia {
    private String apellidos;
    private int NoIntegrantes;
    private int NoPerTrabajadoras;
    private int NoHombres;
    private int NoMujeres;

    public Familia() {
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getNoIntegrantes() {
        return NoIntegrantes;
    }

    public void setNoIntegrantes(int noIntegrantes) {
        NoIntegrantes = noIntegrantes;
    }

    public int getNoPerTrabajadoras() {
        return NoPerTrabajadoras;
    }

    public void setNoPerTrabajadoras(int noPerTrabajadoras) {
        NoPerTrabajadoras = noPerTrabajadoras;
    }

    public int getNoHombres() {
        return NoHombres;
    }

    public void setNoHombres(int noHombres) {
        NoHombres = noHombres;
    }

    public int getNoMujeres() {
        return NoMujeres;
    }

    public void setNoMujeres(int noMujeres) {
        NoMujeres = noMujeres;
    }
}
