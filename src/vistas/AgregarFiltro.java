package vistas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;

public class AgregarFiltro extends JDialog {
    private JPanel contentPane;
    private JTextField txtCodigo;
    private JTextField txtEstado;
    private JTextField txtPrAgua;
    private JButton agregarButton;
    private JComboBox cboClasificacion;
    private JButton buttonOK;
 //private DefaultTableModel tbFiltro;
    public AgregarFiltro() {
        setLocation(400,100);
        setMinimumSize(new Dimension(600,350));
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

       /* buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });*/

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                limpiarCajas();              }
        });
    }
    public void IngresarFiltro(){
        Filtro NuevoFil = new Filtro();
        CtrlFunciones cf = new CtrlFunciones();




        JOptionPane.showMessageDialog(this, "Colonia ingresada");
    }
    public void limpiarCajas(){
        txtCodigo.setText("");
        txtEstado.setText("");
        txtPrAgua.setText("");


    }
    private void onOK() {
        // add your code here
       // dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        AgregarFiltro dialog = new AgregarFiltro();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
