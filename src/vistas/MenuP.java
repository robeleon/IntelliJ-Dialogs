package vistas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MenuP extends JDialog {
    private JPanel contentPane;
    private JButton agregarPersonaButton;
    private JButton agregarColoniaButton;
    private JButton agregarFamiliaButton;
    private JButton agregarFiltroButton;
    private JComboBox comboBox1;
    private JRadioButton mRadioButton;
    private JRadioButton fRadioButton;
    private JButton buttonOK;
    private JButton buttonCancel;

    public MenuP() {
        setLocation(400,100);
        this.setPreferredSize(new Dimension(500,250));
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        comboBox1.addItem("seleccione persona.!");
        comboBox1.addItem("Roberto");
        comboBox1.addItem("Antonio");
        comboBox1.addItem("Otro");



        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        agregarPersonaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               IngPerM  Per = new IngPerM();
               Per.setVisible(true);

            }
        });
        agregarColoniaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngColonia iC = new IngColonia();
                iC.setVisible(true);
            }
        });
        agregarFamiliaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngFamilia InF = new IngFamilia();
                InF.setVisible(true);
            }
        });
        agregarFiltroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AgregarFiltro AgF = new AgregarFiltro();
                AgF.setVisible(true);
            }
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        MenuP dialog = new MenuP();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
