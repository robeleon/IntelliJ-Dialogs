package vistas;

import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emr on 10/07/2017.
 */
public class CtrlFunciones {
    private List<PerMonitora> listaPersona;
    private List<Familia> listaFamilia;
    private List<Colonia> listaColonia;
    private List<Filtro> listaFiltro;

    public CtrlFunciones() {
        listaPersona = new ArrayList<>();
        listaFamilia = new ArrayList<>();
        listaColonia = new ArrayList<>();
        listaFiltro = new ArrayList<>();
    }

    public List<PerMonitora> getListaPersona() {
        return listaPersona;
    }

    public void setListaPersona(List<PerMonitora> listaPersona) {
        this.listaPersona = listaPersona;
    }

    public List<Familia> getListaFamilia() {
        return listaFamilia;
    }

    public void setListaFamilia(List<Familia> listaFamilia) {
        this.listaFamilia = listaFamilia;
    }

    public List<Colonia> getListaColonia() {
        return listaColonia;
    }

    public void setListaColonia(List<Colonia> listaColonia) {
        this.listaColonia = listaColonia;
    }

    public List<Filtro> getListaFiltro() {
        return listaFiltro;
    }

    public void setListaFiltro(List<Filtro> listaFiltro) {
        this.listaFiltro = listaFiltro;
    }

    public void AgregarPerM(PerMonitora pm) {
        listaPersona.add(pm);

    }

    public void mostrarPerM() {
        for (PerMonitora p : listaPersona) {
            System.out.println("nombre: " + p.getNombre());
            System.out.println("apellido: " + p.getApellido());
            System.out.println("edad: " + p.getEdad());
            System.out.println("direccion: " + p.getDireccion());
        }
    }

    public void AgregarColonia(Colonia cl) {
        listaColonia.add(cl);
    }

    public void mostrarColonia() {
        for (Colonia col : listaColonia) {
            System.out.println("nombre: " + col.getNombre());
            System.out.println("ubicacion: " + col.getUbicacion());
            System.out.println("Extencion Territorial: " + col.getExtencionTerritorial());
            System.out.println("No. Familias : " + col.getNoFamilias());
        }
    }
    public void pruebaDt(){
        System.out.println("test de metodo");
    }
}